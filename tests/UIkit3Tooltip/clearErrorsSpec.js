var uikit3Tooltip = uikit3Tooltip || {};

uikit3Tooltip.clearErrorsSpec = function() {
    beforeEach( function() {
        jasmine.clock().install();
        this.row = helper.uikit3.getRow();
        this.input = this.row.querySelector( 'input' );
    } );

    afterEach( function() {
        jasmine.clock().uninstall();
    } );

    it( 'already has errors', function() {
        var theme = new LiveValidator.themes.UIkit3Tooltip( this.input, { tooltip: { duration: 0 } } );
        theme.tooltip.title = 'Old Error';
        theme.tooltip.show();

        jasmine.clock().tick( 10 );

        expect( theme.tooltip.tooltip[ 0 ].innerHTML ).toBe( '<div class="uk-tooltip-inner">Old Error</div>' );
        expect( theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( true );
        theme.clearErrors();
        jasmine.clock().tick( 10 );
        expect( theme.tooltip.title ).toBe( '' );
        expect( theme.tooltip.tooltip ).toBe( false );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
    } );

    it( 'having no errors', function() {
        var theme = new LiveValidator.themes.UIkit3Tooltip( this.input, { tooltip: { duration: 0 } } );

        expect( theme.tooltip.tooltip ).toBe( undefined );
        theme.clearErrors();
        jasmine.clock().tick( 10 );
        expect( theme.tooltip.title ).toBe( '' );
        expect( theme.tooltip.tooltip ).toBe( false );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
    } );

    // Test for issue #3
    it( 'that the first error is not shown once it is removed', function() {
        var theme = new LiveValidator.themes.UIkit3Tooltip( this.input, { tooltip: { duration: 0 } } );

        theme.tooltip.title =  'First';
        theme.clearErrors();
        jasmine.clock().tick( 10 );
        this.input.focus();
        expect( theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( false );
    } );
};
