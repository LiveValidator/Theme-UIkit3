/**
 * The test suite for the uikit3Tooltip theme "class" (UIkit3Tooltip.js)
 */

/* globals uikit3Tooltip */
describe( 'UIkit3Tooltip theme', function() {
    describe( 'check instantiation', function() {
        uikit3Tooltip.instantiationSpec();
    } );
    describe( 'check `addErrors` when', function() {
        uikit3Tooltip.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        uikit3Tooltip.clearErrorsSpec();
    } );
} );
