var uikit3Tooltip = uikit3Tooltip || {};

uikit3Tooltip.instantiationSpec = function() {
    beforeEach( function() {
        this.options = {
            danger: 'uk-form-danger',
            success: 'uk-form-success',
            parentSelector: '.uk-margin',
            controlsSelector: '.uk-form-controls',
            tooltip: {
                pos: 'bottom-left'
            }
        };
    } );

    it( 'when called without `new`', function() {
        var instance = LiveValidator.themes.UIkit3Tooltip( helper.bareInput() );

        expect( instance.options ).toBeDefined();
        expect( window.options ).toBeUndefined();
    } );

    it( 'when called without options', function() {
        var instance = new LiveValidator.themes.UIkit3Tooltip( helper.bareInput() );

        expect( instance.options ).toEqual( this.options );
    } );

    it( 'when called with options', function() {
        var instance = new LiveValidator.themes.UIkit3Tooltip( helper.bareInput(), { danger: 'fail' } );

        this.options.danger = 'fail';

        expect( instance.options ).toEqual( this.options );
    } );
};
