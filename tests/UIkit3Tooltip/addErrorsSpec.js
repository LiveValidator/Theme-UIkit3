var uikit3Tooltip = uikit3Tooltip || {};

uikit3Tooltip.addErrorsSpec = function() {
    beforeEach( function() {
        jasmine.clock().install();
        this.row = helper.uikit3.getRow();
        this.input = this.row.querySelector( 'input' );
        this.theme = new LiveValidator.themes.UIkit3Tooltip( this.input, { tooltip: { duration: 0 } } );
    } );

    afterEach( function() {
        jasmine.clock().uninstall();
    } );

    it( 'already has errors', function() {
        this.theme.tooltip.title = 'Old Error';
        this.theme.tooltip.show();
        expect( this.theme.tooltip.tooltip[ 0 ].innerHTML ).toBe( '<div class="uk-tooltip-inner">Old Error</div>' );

        this.theme.addErrors( [ 'New Error' ] );
        expect( this.theme.tooltip.tooltip[ 0 ] ).not.toContainText( 'Old Error' );
        expect( this.theme.tooltip.tooltip[ 0 ].innerHTML ).toBe( '<div class="uk-tooltip-inner">New Error</div>' );
        expect( this.input ).toHaveClass( 'uk-form-danger' );
    } );

    it( 'having no errors', function() {
        this.theme.tooltip.show();
        expect( this.theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( false );

        this.theme.addErrors( [ 'New Error' ] );
        expect( this.theme.tooltip.tooltip[ 0 ].innerHTML ).toBe( '<div class="uk-tooltip-inner">New Error</div>' );
        expect( this.theme.tooltip.tooltip[ 0 ].matches( 'uk-active' ) ).toBe( false );
        expect( this.input ).toHaveClass( 'uk-form-danger' );
    } );

    it( 'adding multiple errors', function() {
        this.theme.tooltip.show();
        expect( this.theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( false );

        this.theme.addErrors( [ 'Error 1', 'Error 2' ] );
        expect( this.theme.tooltip.tooltip[ 0 ].innerHTML )
            .toBe( '<div class="uk-tooltip-inner">Error 1<br>Error 2</div>' );
        expect( this.theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( false );
        expect( this.input ).toHaveClass( 'uk-form-danger' );
    } );

    it( 'adding errors when element does not have focus', function() {
        this.input.focus();
        jasmine.clock().tick( 10 );
        this.input.blur();
        jasmine.clock().tick( 10 );
        expect( this.theme.tooltip.tooltip ).toBe( false );

        this.theme.addErrors( [ 'Error 1', 'Error 2' ] );
        expect( this.theme.tooltip.title ).toBe( 'Error 1<br>Error 2' );
        expect( this.theme.tooltip.tooltip ).toBe( false );
    } );

    it( 'adding errors when element has focus', function() {
        this.input.focus();

        expect( this.theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( false );
        this.theme.addErrors( [ 'Error 1', 'Error 2' ] );
        jasmine.clock().tick( 10 );
        expect( this.theme.tooltip.tooltip[ 0 ].innerHTML )
            .toBe( '<div class="uk-tooltip-inner">Error 1<br>Error 2</div>' );
        expect( this.theme.tooltip.tooltip[ 0 ].matches( '.uk-active' ) ).toBe( true );
    } );
};
