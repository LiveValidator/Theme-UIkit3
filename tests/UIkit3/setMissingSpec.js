var uikit3 = uikit3 || {};

uikit3.setMissingSpec = function() {
    beforeEach( function() {
        this.row = helper.uikit3.getRow();
        this.input = this.row.querySelector( 'input' );
        this.theme = new LiveValidator.themes.UIkit3( this.input );
    } );

    it( 'already missing', function() {
        this.input.classList.add( 'uk-form-danger' );
        expect( this.input ).toHaveClass( 'uk-form-danger' );
        this.theme.setMissing();
        expect( this.input ).toHaveClass( 'uk-form-danger' );
        expect( this.input ).not.toHaveClass( 'uk-form-success' );
    } );

    it( 'not missing', function() {
        this.input.classList.add( 'uk-form-success' );
        expect( this.input ).toHaveClass( 'uk-form-success' );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
        this.theme.setMissing();
        expect( this.input ).toHaveClass( 'uk-form-danger' );
        expect( this.input ).not.toHaveClass( 'uk-form-success' );
    } );
};
