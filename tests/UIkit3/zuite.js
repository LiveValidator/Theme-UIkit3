/**
 * The test suite for the uikit3 theme "class" (UIkit3.js)
 */

/* globals uikit3 */
describe( 'UIkit3 theme', function() {
    describe( 'check instantiation', function() {
        uikit3.instantiationSpec();
    } );
    describe( 'check `markRequired` when', function() {
        uikit3.markRequiredSpec();
    } );
    describe( 'check `unmarkRequired` when', function() {
        uikit3.unmarkRequiredSpec();
    } );
    describe( 'check `setMissing` when', function() {
        uikit3.setMissingSpec();
    } );
    describe( 'check `unsetMissing` when', function() {
        uikit3.unsetMissingSpec();
    } );
    describe( 'check `addErrors` when', function() {
        uikit3.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        uikit3.clearErrorsSpec();
    } );
} );
