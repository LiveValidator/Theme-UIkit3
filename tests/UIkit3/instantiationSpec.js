var uikit3 = uikit3 || {};

uikit3.instantiationSpec = function() {
    beforeEach( function() {
        this.options = {
            danger: 'uk-form-danger',
            success: 'uk-form-success',
            parentSelector: '.uk-margin',
            controlsSelector: '.uk-form-controls'
        };
    } );

    it( 'when called without `new`', function() {
        var instance = LiveValidator.themes.UIkit3( helper.bareInput() );

        expect( instance.options ).toBeDefined();
        expect( window.options ).toBeUndefined();
    } );

    it( 'when called without options', function() {
        var instance = new LiveValidator.themes.UIkit3( helper.bareInput() );

        expect( instance.options ).toEqual( this.options );
    } );

    it( 'when called with options', function() {
        var instance = new LiveValidator.themes.UIkit3( helper.bareInput(), { danger: 'fail' } );

        this.options.danger = 'fail';

        expect( instance.options ).toEqual( this.options );
    } );
};
