var uikit3 = uikit3 || {};

uikit3.clearErrorsSpec = function() {
    beforeEach( function() {
        this.row = helper.uikit3.getRow();
        this.input = this.row.querySelector( 'input' );
        this.controls = this.row.querySelector( '.uk-form-controls' );
        this.theme = new LiveValidator.themes.UIkit3( this.input );
    } );

    it( 'already has errors', function() {
        this.input.classList.add( 'uk-form-danger' );
        var li = document.createElement( 'li' );
        li.innerHTML = 'Old Error';

        var ul = document.createElement( 'ul' );
        ul.classList.add( 'errors' );
        ul.classList.add( 'uk-list' );
        ul.classList.add( 'uk-text-danger' );
        ul.appendChild( li );

        this.controls.appendChild( ul );
        expect( this.row ).toContainHtml( '<ul class="errors uk-list uk-text-danger">' +
        '<li>Old Error</li></ul>' );

        this.theme.clearErrors();
        expect( this.row ).not.toContainText( 'Error' );
        expect( this.row ).not.toContainElement( '.errors' );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
    } );

    it( 'having no errors', function() {
        expect( this.row ).not.toContainElement( '.errors' );
        this.theme.clearErrors();
        expect( this.row ).not.toContainElement( '.errors' );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
    } );
};
