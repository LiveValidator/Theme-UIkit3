var helper = helper || {};
helper.uikit3 = helper.uikit3 || {};

helper.bareInput = function() {
    setFixtures( '<input />' );
    return document.getElementsByTagName( 'input' )[ 0 ];
};

helper.uikit3.getRow = function() {
    setFixtures( '<div class="uk-margin"><label class="uk-form-label">Label</label>' +
    '<div class="uk-form-controls"><input /></div></div>' );

    return document.getElementsByClassName( 'uk-margin' )[ 0 ];
};
