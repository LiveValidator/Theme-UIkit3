/* globals UIkit */

// Get namespace ready
var LiveValidator = LiveValidator || {};
LiveValidator.themes = LiveValidator.themes || {};

LiveValidator.themes.UIkit3Tooltip = function UIkit3TooltipTheme( element, options ) {

    // Scope-safe the object
    if ( !( this instanceof LiveValidator.themes.UIkit3Tooltip ) ) {
        return new LiveValidator.themes.UIkit3Tooltip( element, options );
    }

    // Call parent (UIkit3) constructor
    LiveValidator.themes.UIkit3.call(
        this,
        element,
        LiveValidator.utils.extend(
            {},
            { tooltip: {
                pos: 'bottom-left'
            } },
            options
        )
    );

    this.tooltip = UIkit.tooltip( this.element, this.options.tooltip );
};

// Inherit methods from UIkit3
LiveValidator.themes.UIkit3Tooltip.prototype = Object.create( LiveValidator.themes.UIkit3.prototype );
LiveValidator.themes.UIkit3Tooltip.prototype.constructor = LiveValidator.themes.UIkit3Tooltip;

LiveValidator.themes.UIkit3Tooltip.prototype.clearErrors = function() {

    // Change visuals and internals as is needed
    this.unsetMissing();
    this.tooltip.title = '';
    this.tooltip.hide();
};
LiveValidator.themes.UIkit3Tooltip.prototype.addErrors = function( errors ) {
    errors = errors.join( '<br>' );

    // Get tooltip to manipulate it
    var $tooltip = $( '.uk-tooltip' );

    // Set errors and change internals
    this.tooltip.title = errors;
    $tooltip.find( '.uk-tooltip-inner' ).html( errors );
    this.setMissing();

    if ( !$tooltip.hasClass( this.tooltip.cls ) && this.element === document.activeElement ) {
        this.tooltip.show();
    }
};
