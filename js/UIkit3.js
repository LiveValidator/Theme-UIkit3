// Get namespace ready
var LiveValidator = LiveValidator || {};
LiveValidator.themes = LiveValidator.themes || {};

LiveValidator.themes.UIkit3 = function UIkit3Theme( element, options ) {

    // Scope-safe the object
    if ( !( this instanceof LiveValidator.themes.UIkit3 ) ) {
        return new LiveValidator.themes.UIkit3( element, options );
    }

    this.element = element;
    this.options = LiveValidator.utils.extend(
        {},
        {
            danger: 'uk-form-danger',
            success: 'uk-form-success',
            parentSelector: '.uk-margin',
            controlsSelector: '.uk-form-controls'
        },
        options
    );
    this.asterisk = null;
    this.controls = null;
    this.parentEl = LiveValidator.utils.parentSelector( this.element, this.options.parentSelector );

    if ( this.parentEl ) {
        this.asterisk = this.parentEl.querySelector( 'span.uk-text-danger' );
        this.controls = this.parentEl.querySelector( this.options.controlsSelector );
    }
};

LiveValidator.themes.UIkit3.prototype.markRequired = function() {
    if ( !this.asterisk && this.parentEl ) {
        this.asterisk = document.createElement( 'span' );
        this.asterisk.innerHTML = ' *';
        LiveValidator.utils.addClass( this.asterisk, 'uk-text-danger' );
        LiveValidator.utils.appendChild( this.parentEl.querySelector( 'label' ), this.asterisk );
    }
};
LiveValidator.themes.UIkit3.prototype.unmarkRequired = function() {
    if ( this.parentEl ) {
        LiveValidator.utils.removeChild( this.parentEl.querySelector( 'label' ), 'span' );
        this.asterisk = null;
    }
};
LiveValidator.themes.UIkit3.prototype.setMissing = function() {
    LiveValidator.utils.removeClass( this.element, this.options.success );
    LiveValidator.utils.addClass( this.element, this.options.danger );
};
LiveValidator.themes.UIkit3.prototype.unsetMissing = function() {
    LiveValidator.utils.removeClass( this.element, this.options.danger );
    LiveValidator.utils.addClass( this.element, this.options.success );
};
LiveValidator.themes.UIkit3.prototype.clearErrors = function() {
    this.unsetMissing();
    LiveValidator.utils.removeChild( this.controls, '.errors' );
};
LiveValidator.themes.UIkit3.prototype.addErrors = function( errors ) {

    // Remove old errors
    this.clearErrors();

    // Create ul
    var ul = document.createElement( 'ul' );
    LiveValidator.utils.addClass( ul, 'errors' );
    LiveValidator.utils.addClass( ul, 'uk-list' );
    LiveValidator.utils.addClass( ul, 'uk-text-danger' );

    // Add each error
    errors.forEach( function( error ) {
        var li = document.createElement( 'li' );
        li.innerHTML = error;
        ul.appendChild( li );
    } );

    // Add errors to row
    LiveValidator.utils.appendChild( this.controls, ul );
    this.setMissing();
};
