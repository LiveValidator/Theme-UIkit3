# LiveValidator - Theme UIkit3

[![build status](https://gitlab.com/LiveValidator/Theme-UIkit3/badges/master/build.svg)](https://gitlab.com/LiveValidator/Theme-UIkit3/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Theme-UIkit3/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Theme-UIkit3/commits/master)

UIkit3 themes for LiveValidator. These UIkit3 themes should integrate seamlessly with any project that already uses UIkit3.

Three options exist to show input errors:
- `uk-list` from the list component
- Tooltips via its component

Find the project [home page and docs](https://livevalidator.gitlab.io/).
